<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('dashboard');
// });


Route::get('/', 'DashboardController@index')->name('dashboard');
 Route::get('addCopy{id}', 'BorrowingController@storeSessionCopy')->name('addCopy');
// Route::get('acerca-de', 'acercaDeController@index')->name('acerca-de');


Route::resource('prestamos', 'BorrowingController', ['parameters' => [
    'prestamos' => 'borrowing',
]]);
Route::resource('libros', 'BookController', ['parameters' => [
    'libros' => 'book',
]]);
Route::resource('clientes', 'CostumerController', ['parameters' => [
    'clientes' => 'costumer',
]]);
Route::resource('categorias', 'CategoryController', ['parameters' => [
    'categorias' => 'category',
]]);
Route::resource('editoriales', 'EditorialController', ['parameters' => [
    'editoriales' => 'editorial',
]]);
Route::resource('autores', 'AuthorController', ['parameters' => [
    'autores' => 'author',
]]);
Route::resource('ejemplares', 'CopyController', ['parameters' => [
    'ejemplares' => 'copy',
]]);
