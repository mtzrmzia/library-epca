<div class="form-group">
        {{ Form::label('copy_id', 'Código del Ejemplar:') }}
        {{ Form::select('code', $copies, null, ['class' => 'form-control select2']) }}
</div>
{{-- <div class="form-group">
        {{ Form::label('costumer_id', 'Cliente:') }}
        {{ Form::select('name', $costumers, null, ['class' => 'form-control select2']) }}
</div>

<div class="form-group">
        {{Form::label('title', '*Titulo del libro: ', ['class' => '']) }}
        {{Form::text('title', null, ['class' => 'form-control','id' => 'name']) }}
</div>
<div class="form-group">
        {{Form::label('isbn', '*ISBN: ', ['class' => '']) }}
        {{Form::text('isbn', null, ['class' => 'form-control','id' => 'isbn']) }}
</div> --}}

<div class="form-group">
        {{-- {{ Form::button('Añadir Ejemplar <i class="fa fa-plus"></i>', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }} --}}
        <a class="btn btn-primary" href="{{ route('addCopy',['id'=>3]) }}">Agregar Ejemplar <i class="fas fa-arrow-alt-circle-left"></i></a>
</div>






@section('scripts')
<script>
        $(document).ready(function() {
         $('.select2').select2({
        theme: 'bootstrap4',
        });
});

</script>
@endsection