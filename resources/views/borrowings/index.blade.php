@extends('layout') 
@section('content')

<h2 class="mb-4 mt-4">Prestamos <i class="fas fa-handshake"></i> </h2>

<a class="btn btn-success mt-2 mb-3" href="{{ route('prestamos.create') }}" role="button">Realizar un prestamos<i class="fas fa-plus"></i></a>


<div class="card mb-4">
    <div class="card-body">
        <table id="table-records" class="table table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><i class="fab fa-slack-hash"></i></th>
                    <th>Cliente</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Devolucion</th>
                    <th>Fecha de Entrega</th>
                    <th>Detalles</th>
                    <th class="actions">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($borrowings as $borrowing)
                <tr>
                    <td>{{ $borrowing->id}}</td>
                    <td>{{ $borrowing->costumer->name.' '.$borrowing->costumer->first_surname}}</td>
                    <td>{{ $borrowing->startDate}}</td>
                    <td>{{ $borrowing->finishDate}}</td>
                    <td>{{ $borrowing->realDate}}</td>
                    <td>{{ $borrowing->details}}</td>
                    <td>
                        <a href="{{ route('prestamos.edit',$borrowing->id)}}" class="btn btn-icon btn-info" data-toggle="tooltip" title="Ver información"><i class="fa fa-fw fa-file-alt"></i></a>
                        <a href="{{ route('prestamos.edit',$borrowing->id)}}" class="btn btn-icon btn-warning" data-toggle="tooltip" title="Editar"><i class="fa fa-fw fa-edit"></i></a>
                        <form class="d-inline" action="{{ route('prestamos.destroy', $borrowing->id) }}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-fw fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@stop 
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#table-records').dataTable( {
                "language": {
                "url": "{{ asset('js/Spanish.json') }}"
                 },
                 "ordering": false,
                 "pageLength": 5
            } );
        });
    </script>
@endsection