@extends('layout') 
@section('content')

<h2 class="mb-4 mt-4">Libros <i class="fas fa-book"></i> </h2>

<a class="btn btn-success mt-2 mb-3" href="{{ route('libros.create') }}" role="button">Añadir nuevo libro <i class="fas fa-plus"></i></a>


<div class="card mb-4">
    <div class="card-body">
        <table id="table-records" class="table table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><i class="fab fa-slack-hash"></i></th>
                    <th width="20%">Titulo</th>
                    <th>ISBN</th>
                    <th>Autor</th>
                    <th>Editorial</th>
                    <th>Categoria</th>
                    <th>Ejemplares</th>
                    <th class="actions">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($books as $book)
                <tr>
                    <td>{{ $book->id }}</td>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->isbn }}</td>
                    <td>{{ $book->author->name }}</td>
                    <td>{{ $book->editorial->name }}</td>
                    <td>{{ $book->category->name }}</td>
                    <td>{{ $book->copies->count() }}</td>

                    <td>
                        <a href="{{ route('libros.edit',$book->id)}}" class="btn btn-icon btn-dark" data-toggle="tooltip" title="Ver Ejemplares"><i class="fa fa-fw fa-bookmark"></i></i></a>
                        {{-- <a href="{{ route('libros.edit',$book->id)}}" class="btn btn-icon btn-info" data-toggle="tooltip" title="Ver Libro"><i class="fa fa-fw fa-eye"></i></a> --}}
                        <a href="{{ route('libros.edit',$book->id)}}" class="btn btn-icon btn-warning" data-toggle="tooltip" title="Editar"><i class="fa fa-fw fa-edit"></i></a>
                        <form class="d-inline" action="{{ route('libros.destroy', $book->id) }}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-fw fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@stop
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#table-records').dataTable( {
                "language": {
                "url": "{{ asset('js/Spanish.json') }}"
                 },
                 "ordering": false,
                 "pageLength": 5
            } );
        });
    </script>
@endsection