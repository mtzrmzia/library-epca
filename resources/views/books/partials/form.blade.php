<div class="form-group">
        {{Form::label('title', '*Titulo del libro: ', ['class' => '']) }}
        {{Form::text('title', null, ['class' => 'form-control','id' => 'name']) }}
</div>
<div class="form-group">
        {{ Form::label('author_id', 'Autor:') }} {{ Form::select('author_id', $authors, null, ['class' => 'form-control select2']) }}
</div>
<div class="form-group">
        {{ Form::label('category_id', 'Categoría:') }} {{ Form::select('category_id', $categories, null, ['class' => 'form-control select2']) }}
</div>
<div class="form-group">
        {{ Form::label('editorial_id', 'Editorial:') }} {{ Form::select('editorial_id', $editorials, null, ['class' => 'form-control select2']) }}
</div>

<div class="form-group">
        {{Form::label('isbn', '*ISBN: ', ['class' => '']) }}
        {{Form::text('isbn', null, ['class' => 'form-control','id' => 'isbn']) }}
</div>

<div class="form-group">
        {{ Form::button('Guardar <i class="fa fa-save"></i>', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
        <a class="btn btn-danger" href="{{ route('libros.index') }}">Volver <i class="fas fa-arrow-alt-circle-left"></i></a>
</div>






@section('scripts')
<script>
        $(document).ready(function() {
         $('.select2').select2({
        theme: 'bootstrap4',
        });
});

</script>
@endsection