@extends('layout') 
@section('content')
<h2 class="mb-4 mt-4">Añadir Cliente <i class="fas fa-glasses"></i> </h2>

<div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
        Campos con * son obligatorios.
    </div>
    <div class="card-body">
            {!! Form::open(['route' => 'clientes.store','class' => 'form-horizontal','autocomplete' => 'off']) !!}
                        
            @include('costumers.partials.form')

        {!! Form::close() !!}
    </div>
</div>

@stop