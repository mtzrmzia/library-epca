<div class="form-group">
    {{Form::label('name', '*Nombre del Cliente: ', ['class' => '']) }} {{Form::text('name', null, ['class' => 'form-control',
    'id' => 'name']) }}
</div>

<div class="form-group">
    {{Form::label('first_surname', '*Apellido Paterno: ', ['class' => '']) }} {{Form::text('first_surname', null, ['class' =>
    'form-control', 'id' => 'first_surname']) }}
</div>

<div class="form-group">
    {{Form::label('second_surname', 'Apellido Materno: ', ['class' => '']) }} {{Form::text('second_surname', null, ['class' =>
    'form-control', 'id' => 'second_surname']) }}
</div>

<div class="form-group">
    {{Form::label('phone', '*Teléfono del Cliente: ', ['class' => '']) }} {{Form::text('phone', null,['class' => 'form-control',
    'id' => 'phone','placeholder' => '(4771234567)']) }}
</div>

<div class="form-group">
    {{Form::label('email', '*Email del Cliente: ', ['class' => '']) }} {{Form::email('email', null,['class' => 'form-control',
    'id' => 'email']) }}
</div>

<div class="form-group">
    {{ Form::button('Guardar <i class="fa fa-save"></i>', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
    <a class="btn btn-danger" href="{{ route('clientes.index') }}">Volver <i class="fas fa-arrow-alt-circle-left"></i></a>
</div>