@extends('layout') 
@section('content')

<h2 class="mb-4 mt-4">Ejemplares <i class="fas fa-bookmark"></i> </h2>

<a class="btn btn-success mt-2 mb-3" href="{{ route('ejemplares.create') }}" role="button">Añadir nuevo Ejemplar <i class="fas fa-plus"></i></a>


<div class="card mb-4">
    <div class="card-body">
        <table id="table-records" class="table table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><i class="fab fa-slack-hash"></i></th>
                    <th>Libro</th>
                    <th>Código</th>
                    <th>Status</th>
                    <th>Detalles</th>
                    <th class="actions">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($copies as $copy)
                <tr>
                    <td>{{ $copy->id}}</td>
                    <td>{{ $copy->book->title}}</td>
                    <td>{{ $copy->code}}</td>
                    <td><span class="badge @if($copy->status == 'DISPONIBLE') badge-success @else badge-danger @endif">{{ $copy->status}}</span></td>
                    <td>{{ $copy->details}}</td>
                    <td>
                        <a href="{{ route('ejemplares.edit',$copy->id)}}" class="btn btn-icon btn-warning" data-toggle="tooltip" title="Editar"><i class="fa fa-fw fa-edit"></i></a>
                        <form class="d-inline" action="{{ route('ejemplares.destroy', $copy->id) }}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-fw fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
   
@stop
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#table-records').dataTable( {
                "language": {
                "url": "{{ asset('js/Spanish.json') }}"
                 },
                 "ordering": false,
                 "pageLength": 5
            } );
        });
    </script>
@endsection