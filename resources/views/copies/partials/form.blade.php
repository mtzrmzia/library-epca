<div class="form-group">
        {{ Form::label('book_id', 'Selecciona el Libro:') }}
        {{ Form::select('book_id', $books, null, ['class' => 'form-control select2']) }}
</div>

<div class="form-group">
        {{Form::label('code', '*Código: ', ['class' => '']) }}
        {{Form::text('code', null, ['class' => 'form-control','id' => 'code']) }}
</div>
<div class="form-group">
        {{Form::label('details', 'Detalles: ', ['class' => '']) }}
        {{Form::textarea('details', null, ['class' => 'form-control','id' => 'details','rows'=>'4']) }}
</div>
<div class="form-group">
        {{ Form::label('status', 'Estado:') }}
        <label>
            {{ Form::radio('status', 'DISPONIBLE',true) }} Disponible
        </label>
        <label>
            {{ Form::radio('status', 'PRESTADO') }} Prestado
        </label>
    </div>
<div class="form-group">
        {{ Form::button('Guardar <i class="fa fa-save"></i>', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
        <a class="btn btn-danger" href="{{ route('ejemplares.index') }}">Volver <i class="fas fa-arrow-alt-circle-left"></i></a>
</div>






@section('scripts')
<script>
        $(document).ready(function() {
         $('.select2').select2({
        theme: 'bootstrap4',
        });
});

</script>
@endsection