@extends('layout') 
@section('content')
<h2 class="mb-4 mt-4">Editar Categoría <i class="fas fa-sad-cry"></i> </h2>

<div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
        Campos con * son obligatorios.
    </div>
    <div class="card-body">
        {!! Form::model($category, ['route' => ['categorias.update',$category->id], 'method' => 'PUT','autocomplete' => 'off']) !!}
                        
            @include('categories.partials.form')

        {!! Form::close() !!}
    </div>
</div>

@stop