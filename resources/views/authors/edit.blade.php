@extends('layout') 
@section('content')
<h2 class="mb-4 mt-4">Editar Autor <i class="fas fa-pen-fancy"></i> </h2>

<div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
        Campos con * son obligatorios.
    </div>
    <div class="card-body">
        {!! Form::model($author, ['route' => ['autores.update', $author->id], 'method' => 'PUT','autocomplete' => 'off']) !!}
                        
            @include('authors.partials.form')

        {!! Form::close() !!}
    </div>
</div>

@stop