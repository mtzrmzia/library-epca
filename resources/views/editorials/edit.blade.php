@extends('layout') 
@section('content')
<h2 class="mb-4 mt-4">Editar Editorial <i class="fas fa-atlas"></i> </h2>

<div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
        Campos con * son obligatorios.
    </div>
    <div class="card-body">
            {!! Form::model($editorial, ['route' => ['editoriales.update',$editorial->id], 'method' => 'PUT','autocomplete' => 'off']) !!}
                        
            @include('editorials.partials.form')

        {!! Form::close() !!}
    </div>
</div>

@stop