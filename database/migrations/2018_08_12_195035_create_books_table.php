<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',200);
            $table->integer('author_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('editorial_id')->unsigned();
            $table->string('isbn',45);
            $table->string('slug');

            // Relaciones

            $table->foreign('author_id')->references('id')->on('authors')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('editorial_id')->references('id')->on('editorials')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
