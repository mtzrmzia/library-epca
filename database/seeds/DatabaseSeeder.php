<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AuthorsTableSeeder::class);
        $this->call(EditorialsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CostumersTableSeeder::class);
        $this->call(BooksTableSeeder::class);
        $this->call(CopiesTableSeeder::class);
        $this->call(BorrowingsTableSeeder::class);
    }
}
