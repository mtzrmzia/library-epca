<?php

use Faker\Generator as Faker;

$factory->define(App\Copy::class, function (Faker $faker) {

    $code = generateRandomString(13);
    return [

        'book_id'       =>rand(1,15),
        'code'          =>$code,
        'status'        =>$faker->randomElement(['DISPONIBLE', 'PRESTADO']),
        'details'       =>$faker->sentence(5),
        'slug'          =>str_slug($code),
    ];
});


function generateRandomString($length) { 
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
} 