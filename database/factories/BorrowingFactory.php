<?php

use Faker\Generator as Faker;

$factory->define(App\Borrowing::class, function (Faker $faker) {
    return [
        'costumer_id'   =>rand(1,20),
        'startDate'     =>$faker->date('Y-m-d','now'),
        'finishDate'    =>$faker->date('Y-m-d','now'),
        'realDate'      =>$faker->date('Y-m-d','now'),
        'details'       =>$faker->sentence(5),
    ];
});
